#########################
# Create base container #
#########################
FROM ubuntu:24.04 as base
LABEL maintainer="pidydx"

# Base setup
ENV APP_USER=valheim
ENV APP_GROUP=valheim

ENV BASE_PKGS language-pack-en lib32gcc-s1-amd64-cross libc6-i386 libatomic1 libpulse-dev libpulse0 software-properties-common

RUN dpkg --add-architecture i386 \
 && echo 'LANG="en_US.UTF-8"' > /etc/default/locale \
 && echo "en_US.UTF-8 UTF-8" >> /etc/locale.gen

ENV LD_LIBRARY_PATH ${LD_LIBRARY_PATH}:/usr/x86_64-linux-gnu/lib32

# Update users and groups
RUN userdel ubuntu
RUN groupadd -g 1000 ${APP_GROUP} \
 && useradd -m -N -u 1000 ${APP_USER} -g ${APP_GROUP} -d /usr/local/valheim

# Update and install base packages
RUN apt-get update -q \
 && DEBIAN_FRONTEND=noninteractive apt-get upgrade -yq \
 && DEBIAN_FRONTEND=noninteractive apt-get install -yq --no-install-recommends ca-certificates \
 && apt-get update -q \
 && DEBIAN_FRONTEND=noninteractive apt-get install -yq --no-install-recommends ${BASE_PKGS} \
 && rm -rf /var/lib/apt/lists/*


##########################
# Create build container #
##########################
FROM base AS builder

# Set build dependencies
ENV BUILD_DEPS wget

RUN echo steam steam/question select "I AGREE" | debconf-set-selections \
 && echo steam steam/license note '' | debconf-set-selections

RUN apt-get update -q \
&& DEBIAN_FRONTEND=noninteractive apt-get install -yq --no-install-recommends ${BUILD_DEPS}

# Run build
WORKDIR /usr/src

RUN mkdir -p /usr/local/valheim
RUN chown ${APP_USER}:${APP_GROUP} /usr/local/valheim
RUN wget http://media.steampowered.com/installer/steamcmd_linux.tar.gz
RUN tar -zxf steamcmd_linux.tar.gz -C /usr/local/valheim
RUN ldconfig
USER $APP_USER
RUN /usr/local/valheim/steamcmd.sh +force_install_dir /usr/local/valheim/server +login anonymous +app_update 896660 validate +exit


##########################
# Create final container #
##########################
FROM base

# Prepare container
COPY --from=builder /usr/local /usr/local/
COPY usr/ /usr/

ENV LD_LIBRARY_PATH=/usr/local/valheim/linux64:$LD_LIBRARY_PATH
ENV SteamAppId=892970
ENV SERVER_NAME="bf-valheim"
ENV SERVER_PASS="valheim"
ENV WORLD_NAME="Libertalia"

RUN mkdir -p /var/lib/valheim \
 && chown ${APP_USER}:${APP_GROUP} /var/lib/valheim

EXPOSE 2456/udp 2457/udp 2458/udp

VOLUME ["/var/lib/valheim"]

USER $APP_USER

ENTRYPOINT ["docker-entrypoint.sh"]
CMD ["valheim"]

