#!/bin/bash

set -e

if [ "$1" = 'valheim' ]; then
    exec /usr/local/valheim/server/valheim_server.x86_64 -name "${SERVER_NAME}" -password "${SERVER_PASS}" -world "${WORLD_NAME}" -savedir /var/lib/valheim -public 0 -crossplay ${PRESET} ${MODIFIERS} ${SETKEYS}
fi

exec "$@"
